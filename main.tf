provider "aws"{
   region = var.region
}

resource "aws_instance" "my_instance" {
   ami = var.ami
   instance_type = var.inst_type
   tags = var.inst_tag
   vpc_security_group_ids = [aws_security_group.my_sg.id]
}

resource "aws_security_group" "my_sg" {
   name = "instance_sg"
   dynamic "ingress" {
         for_each = var.my_ports
         content {
		from_port   = ingress.value
		to_port     = ingress.value
		protocol    = "tcp"
		cidr_blocks = var.my_cidr
		}
	}
   egress {
 	from_port = 0
        to_port   = 0
        protocol  = "tcp"
        cidr_blocks = var.my_cidr
        }
}

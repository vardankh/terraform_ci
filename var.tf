variable "region" {
   default = "eu-west-1"
}

variable "ami" {
   default = "ami-04dd4500af104442f"
}

variable "inst_type" {
   default = "t2.micro"
}

variable "my_ports" {
   type = list
   default = ["80","443","22"]
}

variable "inst_tag" {
   type = map
   default = {
        Name = "my_instance_TR"
        Owner = "Vardan"
        Env  = "test"
      }
}

variable "my_cidr" {
   default = ["0.0.0.0/0"]
}
